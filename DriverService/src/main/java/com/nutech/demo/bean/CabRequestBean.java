package com.nutech.demo.bean;

import java.io.Serializable;

/**
 * @author Hetesh Mohan
 *
 */
public class CabRequestBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String profileId;

	private String contactNumber;


	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

}
