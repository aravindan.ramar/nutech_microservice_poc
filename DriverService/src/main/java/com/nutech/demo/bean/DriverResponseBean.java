package com.nutech.demo.bean;

import java.io.Serializable;

/**
 * @author Hetesh Mohan
 *
 */
public class DriverResponseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private int requestId;
	private String profileId;
	private String userProfileId;

	private DriverUserBean driverUserBean;

	public String getUserProfileId() {
		return userProfileId;
	}

	public void setUserProfileId(String userProfileId) {
		this.userProfileId = userProfileId;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public DriverUserBean getDriverUserBean() {
		return driverUserBean;
	}

	public void setDriverUserBean(DriverUserBean driverUserBean) {
		this.driverUserBean = driverUserBean;
	}

}
