package com.nutech.demo.bean;

/**
 * @author Hetesh Mohan
 *
 */
public class DriverAcceptanceBean {

	private String profileid;

	private boolean status;

	private int requestId;

	

	public String getProfileid() {
		return profileid;
	}

	public void setProfileid(String profileid) {
		this.profileid = profileid;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

}
