package com.nutech.demo.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.nutech.demo.configuration.SequenceGenerator;

/**
 * @author Hetesh Mohan
 * @desc driver user bean
 *
 */

@Entity
@Table(name = "Driver_profile")
public class DriverUserBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 255)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "driver_seq")
    @GenericGenerator(
        name = "driver_seq", 
        strategy = "com.nutech.demo.configuration.SequenceGenerator", 
        
        parameters = {		
        	@Parameter(name = SequenceGenerator.INCREMENT_PARAM, value = "50"),
            @Parameter(name = SequenceGenerator.VALUE_PREFIX_PARAMETER, value = "DRIVER_"),
            @Parameter(name = SequenceGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	private String userid;

	@Column(name = "name")
	private String name;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "gender")
	private String gender;

	@Column(name = "address")
	private String address;

	@Column(name = "contact_number")
	private String contactNumber;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

}
