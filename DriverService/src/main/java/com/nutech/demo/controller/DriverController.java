package com.nutech.demo.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.nutech.demo.bean.DriverAcceptanceBean;
import com.nutech.demo.bean.DriverUserBean;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.ProfileRequestBean;
import com.nutech.demo.configuration.LoggerImpl;
import com.nutech.demo.service.DriverService;

/**
 * @author Hetesh Mohan
 *
 */
@RestController
@RequestMapping(value = { "/driver" })
public class DriverController {
	@Autowired
	DriverService driverService;
	@Autowired
	HttpSession session;

	/**
	 * @description Create Driver user
	 * @param user
	 * @param ucBuilder
	 * @return response
	 */
	@PostMapping(value = "/create", headers = "Accept=application/json")
	public ResponseEntity<String> createUser(@RequestBody DriverUserBean user, UriComponentsBuilder ucBuilder) {
		ResponseEntity<String> response = null;
		DriverUserBean userExist = driverService.findByUsername(user.getUsername());
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getUserid()).toUri());
		if (userExist == null) {
			driverService.createUser(user);
			response = new ResponseEntity<String>("Registered Successfully", headers, HttpStatus.CREATED);
		} else {
			response = new ResponseEntity<String>("Already Exists", headers, HttpStatus.CONFLICT);
		}

		return response;
	}

	/**
	 * @description Driver accept the user request
	 * @param driverAcceptanceBean
	 * @return
	 */
	@PostMapping(value = "/acceptUserRequest", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> accept(@RequestBody DriverAcceptanceBean driverAcceptanceBean) {
		Boolean responseMessage = driverService.accept(driverAcceptanceBean);
		if (responseMessage == null) {
			return new ResponseEntity<Boolean>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Boolean>(responseMessage, HttpStatus.OK);
	}

	/**
	 * @description Login the Driver profile
	 * @param loginBean
	 * @param ucBuilder
	 * @return
	 */
	@PostMapping(value = "/login", headers = "Accept=application/json")
	public ResponseEntity<String> loginUser(@RequestBody LoginBean loginBean, UriComponentsBuilder ucBuilder) {
		LoggerImpl.info("Login Driver profile :: " + loginBean.getUsername());
		DriverUserBean userBean = driverService.loginUser(loginBean);
		String loginStatus = userBean != null ? "success" : "failure";
		LoggerImpl.info("Login result :: " + loginStatus);
		HttpHeaders headers = new HttpHeaders();
		session.setAttribute("userId", userBean.getUserid());
		headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(userBean.getUserid()).toUri());
		return new ResponseEntity<String>(loginStatus, headers, HttpStatus.OK);
	}

	/**
	 * @description Get cab requests from user
	 * @return requestLists
	 */
	@GetMapping(value = "/getCabRequests", headers = "Accept=application/json")
	public List<ProfileRequestBean> getRequest() {
		List<ProfileRequestBean> requestLists = null;
		if (session.getAttribute("userId") != null) {
			requestLists = driverService.getAllRequest();
		}
		return requestLists;

	}

	/**
	 * @description Logout method
	 * @return res
	 */
	@GetMapping(value = "/logout", headers = "Accept=application/json")
	public String logout() {
		String res = "Please Login first..";
		if (session.getAttribute("userId") != null) {
			session.invalidate();
			res = "Logged Out success..";
		}
		return res;
	}

}
