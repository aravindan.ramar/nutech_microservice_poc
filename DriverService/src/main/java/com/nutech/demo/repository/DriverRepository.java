package com.nutech.demo.repository;

import java.util.List;

import com.nutech.demo.bean.DriverAcceptanceBean;
import com.nutech.demo.bean.DriverUserBean;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.ProfileRequestBean;

/**
 * @author Hetesh Mohan
 *
 */
public interface DriverRepository {

	public Boolean accept(DriverAcceptanceBean driverAcceptanceBean);

	public void createDriverUser(DriverUserBean user);

	public DriverUserBean findByUsername(String userName);

	public DriverUserBean loginDriver(LoginBean loginBean);

	public List<ProfileRequestBean> getAllRequest();

	public ProfileRequestBean findById(int id);

}
