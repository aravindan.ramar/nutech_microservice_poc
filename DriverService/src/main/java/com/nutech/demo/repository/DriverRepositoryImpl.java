package com.nutech.demo.repository;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nutech.demo.bean.CabRequestBean;
import com.nutech.demo.bean.DriverAcceptanceBean;
import com.nutech.demo.bean.DriverResponseBean;
import com.nutech.demo.bean.DriverUserBean;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.ProfileRequestBean;
import com.nutech.demo.configuration.LoggerImpl;

/**
 * @author Hetesh mohan
 *
 */
@Repository
public class DriverRepositoryImpl implements DriverRepository {
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	HttpSession userSession;

	@Override
	public Boolean accept(DriverAcceptanceBean driverAcceptanceBean) {
		ProfileRequestBean bean = this.findById(driverAcceptanceBean.getRequestId());
		boolean res = false;
		if (driverAcceptanceBean.isStatus() == true) {
			try {
				DriverResponseBean driverResponse = new DriverResponseBean();
				bean.setStatus(false);
				Session session = sessionFactory.getCurrentSession();
				session.save(bean);
				String userId = userSession.getAttribute("userId").toString();
				DriverUserBean driverDetails = this.getDriverDetails(userId);
				driverResponse.setDriverUserBean(driverDetails);
				driverResponse.setProfileId(userId);
				driverResponse.setRequestId(bean.getRequestId());
				driverResponse.setUserProfileId(bean.getProfileId());
				jmsTemplate.convertAndSend("response-queue", driverResponse);
				res = true;

			} catch (Exception e) {
				LoggerImpl.warn(e.getMessage());
			}

		}
		return res;
	}

	public DriverUserBean getDriverDetails(String userId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from DriverUserBean where userid=:profileid");
		query.setParameter("profileid", userId);
		return (DriverUserBean) query.uniqueResult();
	}

	@JmsListener(destination = "profile-queue")
	@Transactional
	public void receive(CabRequestBean user) {
		try {
			ProfileRequestBean userBean = new ProfileRequestBean();
			userBean.setStatus(true);
			userBean.setProfileId(user.getProfileId());
			userBean.setContactNumber(user.getContactNumber());
			this.send(userBean);

		} catch (Exception e) {
		LoggerImpl.warn(e.getMessage());
		}

	}

	@Override
	public void createDriverUser(DriverUserBean user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
	}

	public void send(ProfileRequestBean userBean) {
		Session session = sessionFactory.getCurrentSession();
		session.save(userBean);
	}

	@Override
	public DriverUserBean findByUsername(String userName) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from DriverUserBean where username=:username");
		query.setParameter("username", userName);
		return (DriverUserBean) query.uniqueResult();
	}

	@Override
	public DriverUserBean loginDriver(LoginBean loginBean) {
		Session session = sessionFactory.getCurrentSession();
		Criteria loginCriteria = session.createCriteria(DriverUserBean.class);
		Criterion usernameCriterion = Restrictions.eq("username", loginBean.getUsername());
		Criterion passwordCriterion = Restrictions.eq("password", loginBean.getPassword());
		loginCriteria.add(Restrictions.and(usernameCriterion, passwordCriterion));
		DriverUserBean user = (DriverUserBean) loginCriteria.uniqueResult();
		return user;
	}

	@Override
	public List<ProfileRequestBean> getAllRequest() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from ProfileRequestBean where status=true");
		List<ProfileRequestBean> requestlist = query.getResultList();
		return requestlist;
	}

	@Override
	public ProfileRequestBean findById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from ProfileRequestBean where requestId=:requestId");
		query.setParameter("requestId", id);
		return (ProfileRequestBean) query.uniqueResult();
	}

}
