package com.nutech.demo.service;

import java.util.List;

import com.nutech.demo.bean.DriverAcceptanceBean;
import com.nutech.demo.bean.DriverUserBean;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.ProfileRequestBean;

/**
 * @author Hetesh Mohan
 *
 */
public interface DriverService {
	public Boolean accept(DriverAcceptanceBean driverAcceptanceBean);

	public void createUser(DriverUserBean user);

	public DriverUserBean findByUsername(String userName);

	public DriverUserBean loginUser(LoginBean loginBean);

	public List<ProfileRequestBean> getAllRequest();
}
