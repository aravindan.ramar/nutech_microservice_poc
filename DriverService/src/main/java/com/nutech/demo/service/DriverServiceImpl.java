package com.nutech.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nutech.demo.bean.DriverAcceptanceBean;
import com.nutech.demo.bean.DriverUserBean;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.ProfileRequestBean;
import com.nutech.demo.repository.DriverRepository;

/**
 * @author Hetesh Mohan
 *
 */
@Service
@Transactional
public class DriverServiceImpl implements DriverService {
	@Autowired
	DriverRepository driverRepository;

	@Override
	public Boolean accept(DriverAcceptanceBean driverAcceptanceBean) {
		return driverRepository.accept(driverAcceptanceBean);
	}

	@Override
	public void createUser(DriverUserBean user) {
		driverRepository.createDriverUser(user);
	}

	@Override
	public DriverUserBean findByUsername(String userName) {
		DriverUserBean userBean;
		userBean = driverRepository.findByUsername(userName);
		return userBean;
	}

	@Override
	public DriverUserBean loginUser(LoginBean loginBean) {

		DriverUserBean userBean;
		userBean = driverRepository.loginDriver(loginBean);
		return userBean;
	}

	@Override
	public List<ProfileRequestBean> getAllRequest() {
		List<ProfileRequestBean> request = driverRepository.getAllRequest();
		return request;
	}
}
