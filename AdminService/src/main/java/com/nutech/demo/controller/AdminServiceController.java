package com.nutech.demo.controller;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.nutech.demo.bean.DriverDetailRepoBean;
import com.nutech.demo.bean.UploadFileResponse;
import com.nutech.demo.configuration.LoggerImpl;
import com.nutech.demo.service.AdminService;
import com.nutech.demo.service.FileStorageService;

/**
 * @author Hetesh
 * 
 */
@RestController
public class AdminServiceController {
	Logger logger = LoggerFactory.getLogger(AdminServiceController.class);
	@Autowired
	private FileStorageService fileStorageService;
	@Autowired
	private AdminService adminService;

	@PostMapping("/uploadLicenceImage")
	public boolean uploadLicenceImage(@RequestParam("LicenceImage") MultipartFile file, @RequestHeader(value = "UserId") String UserId)
			throws IOException {
		boolean status = false;
		byte[] image = file.getBytes();
		int userId = Integer.parseInt(UserId);
		status = adminService.addLicenceImage(image, userId);
		return status;
	}
	
	@PostMapping("/uploadProfileImage")
	public boolean uploadPofileImage(@RequestParam("ProfileImage") MultipartFile file, @RequestHeader(value = "UserId") String UserId)
			throws IOException {
		boolean status = false;
		byte[] image = file.getBytes();
		int userId = Integer.parseInt(UserId);
		status = adminService.addProfileImage(image, userId);
		return status;
	}
	
	
	/**
	 * @param driverDetails
	 * @return response
	 * @see this method is to create the Driver details
	 */
	@PostMapping("/addDriver")
	public ResponseEntity<String> addDriver(@RequestBody DriverDetailRepoBean driverDetails) {
		boolean status = false;
		ResponseEntity<String> response = null;
		status = adminService.addDriveruser(driverDetails);
		if (status == true) {
			logger.info("add driver:: driver name" + driverDetails.getName());
			response = new ResponseEntity<String>("Registered Successfully", HttpStatus.CREATED);
		} else {
			response = new ResponseEntity<String>("failed to registrer", HttpStatus.CONFLICT);
		}
		return response;
	}

	@GetMapping("/getDriverDetails/{id}")
	public  DriverDetailRepoBean getDriverDetails(@PathVariable int id) {
		DriverDetailRepoBean bean  = adminService.getDriverDetails(id);
		LoggerImpl.info("hetesh+Moah=an=_+++++++++++++============================");
		return bean;
	}
}
