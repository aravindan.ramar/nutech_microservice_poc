package com.nutech.demo.bean;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DrivingLicenceImage")
public class DriverLicenceImageBean {
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="UserId")
	private int userId;
	
	@Column(name="LicenceImage")
	private Blob licenceImageImage;

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Blob getLicenceImageImage() {
		return licenceImageImage;
	}
	public void setLicenceImageImage(Blob licenceImageImage) {
		this.licenceImageImage = licenceImageImage;
	}
	
}
