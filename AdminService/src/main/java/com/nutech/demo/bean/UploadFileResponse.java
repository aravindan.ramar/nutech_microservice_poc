package com.nutech.demo.bean;

/**
 * @author Hetesh
 *
 */
public class UploadFileResponse {
	private String fileName ;
	private String fileURL;
	private String type;
	private long size;

	public UploadFileResponse(String profilePhotoFile, String fileDownloadUri, String contentType, long size) {
		
		this.fileName = profilePhotoFile;
		this.fileURL = fileDownloadUri;
		this.type = contentType;
		this.size = size;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileURL() {
		return fileURL;
	}

	public void setFileURL(String fileURL) {
		this.fileURL = fileURL;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}
	
	

}
