package com.nutech.demo.bean;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import com.nutech.demo.configuration.SequenceGenerator;

/**
 * @author Hetesh
 *
 */
@Entity
@Table(name="Driver_Details")
@SecondaryTable(name="car_Details")
public class DriverDetailRepoBean {
	
	@Id
	@Column(length = 255)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Driver_seq")
    @GenericGenerator(
        name = "Driver_seq", 
        strategy = "com.nutech.demo.configuration.SequenceGenerator", 
        
        parameters = {		
        	@Parameter(name = SequenceGenerator.INCREMENT_PARAM, value = "50"),
            @Parameter(name = SequenceGenerator.VALUE_PREFIX_PARAMETER, value = "DRIVER_"),
            @Parameter(name = SequenceGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	private String driverId;
	
	@Column(name="name")
	private String name;
	@Column(name="age")
	private int age;
	@Column(name="licence_no")
	private String licenceNo;
	@Column(name="gender")
	private char gender;
	
	@OneToOne(cascade = CascadeType.ALL)
	private CarBean carbean;
	
	public CarBean getCarbean() {
		return carbean;
	}

	public void setCarbean(CarBean carbean) {
		this.carbean = carbean;
	}

	
	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getLicenceNo() {
		return licenceNo;
	}

	public void setLicenceNo(String licenceNo) {
		this.licenceNo = licenceNo;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

}
