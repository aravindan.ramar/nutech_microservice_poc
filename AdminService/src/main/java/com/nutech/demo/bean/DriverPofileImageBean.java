package com.nutech.demo.bean;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DrivingProfileImage")
public class DriverPofileImageBean {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="UserId")
	private int userId;
	
	@Column(name="ProfileImage")
	private Blob ProfileImageImage;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Blob getProfileImageImage() {
		return ProfileImageImage;
	}

	public void setProfileImageImage(Blob profileImageImage) {
		ProfileImageImage = profileImageImage;
	}

}