package com.nutech.demo.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="car_Details")
public class CarBean {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@Column(name="driverId")
	private int driverId;
	@Column(name="carNumber")
	private String carNumber;
	@Column(name="maker")
	private String maker;
	@Column(name="carType")
	private String carType;
	@OneToOne
	@PrimaryKeyJoinColumn
	DriverDetailRepoBean driverBean;
	public DriverDetailRepoBean getDriverBean() {
		return driverBean;
	}
	public void setDriverBean(DriverDetailRepoBean driverBean) {
		this.driverBean = driverBean;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDriverId() {
		return driverId;
	}
	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}
	public String getCarNumber() {
		return carNumber;
	}
	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}
	public String getMaker() {
		return maker;
	}
	public void setMaker(String maker) {
		this.maker = maker;
	}
	public String getCarType() {
		return carType;
	}
	public void setCarType(String carType) {
		this.carType = carType;
	}
	

}
