package com.nutech.demo.service;

import java.sql.Blob;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nutech.demo.bean.DriverDetailRepoBean;
import com.nutech.demo.bean.DriverLicenceImageBean;
import com.nutech.demo.bean.DriverPofileImageBean;
import com.nutech.demo.repository.AdminRepository;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	AdminRepository adminRepository;

	@Override
	public boolean addDriveruser(DriverDetailRepoBean driverDetails) {
		boolean status = false;
		status = adminRepository.addDriver(driverDetails);
		return status;
	}

	@Override
	public boolean addLicenceImage(byte[] image, int userId) {
		DriverLicenceImageBean driverLicenceImage = new DriverLicenceImageBean();
		boolean status = false;
		Blob imageBlob ;
		try {
			 imageBlob = new SerialBlob(image);
			 driverLicenceImage.setLicenceImageImage(imageBlob);
			 driverLicenceImage.setUserId(userId);
		} catch (SerialException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		status = adminRepository.addLicenceImage(driverLicenceImage);
		return status;
	}

	@Override
	public boolean addProfileImage(byte[] image, int userId) {
		DriverPofileImageBean driverPofileImage = new DriverPofileImageBean();
		boolean status = false;
		Blob imageBlob ;
		try {
			 imageBlob = new SerialBlob(image);
			 driverPofileImage.setProfileImageImage(imageBlob);
			 driverPofileImage.setUserId(userId);
		} catch (SerialException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		status = adminRepository.addProfileImage(driverPofileImage);
		return status;
		
		
	}

	@Override
	public DriverDetailRepoBean getDriverDetails(int id) {
		DriverDetailRepoBean bean = new DriverDetailRepoBean();
		bean = adminRepository.getDriverDetails(id);
		return bean;
	}
	
	

}
