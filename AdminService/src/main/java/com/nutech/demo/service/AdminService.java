package com.nutech.demo.service;

import org.springframework.stereotype.Service;

import com.nutech.demo.bean.DriverDetailRepoBean;
import com.nutech.demo.bean.drievrDetailBean;

@Service

public interface AdminService {


	public boolean addDriveruser(DriverDetailRepoBean driverDetails);

	public boolean addLicenceImage(byte[] image, int userId);

	public boolean addProfileImage(byte[] image, int userId);

	public DriverDetailRepoBean getDriverDetails(int id);
}

