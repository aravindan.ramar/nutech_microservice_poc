package com.nutech.demo.repository;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nutech.demo.bean.DriverDetailRepoBean;
import com.nutech.demo.bean.DriverLicenceImageBean;
import com.nutech.demo.bean.DriverPofileImageBean;
import com.nutech.demo.configuration.LoggerImpl;

@Repository
@Transactional
public class AdminRepositoryImpl implements AdminRepository {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public boolean addDriver(DriverDetailRepoBean bean) {
		boolean status = false;
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(bean);
			status = true;
		} catch (Exception e) {
			LoggerImpl.warning("addDriver ::" + e.getMessage());
		}
		return status;
	}

	@Override
	public boolean addLicenceImage(DriverLicenceImageBean driverLicenceImage) {
		boolean status = false;
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(driverLicenceImage);
			status = true;
		} catch (Exception e) {
			LoggerImpl.warning("addLicenceImage::" + e.getMessage());
		}
		return status;
	}

	@Override
	public boolean addProfileImage(DriverPofileImageBean driverPofileImage) {
		boolean status = false;
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(driverPofileImage);
			status = true;
		} catch (Exception e) {
			LoggerImpl.warning("addLicenceImage::" + e.getMessage());
		}
		return status;
	}

	@Override
	public DriverDetailRepoBean getDriverDetails(int id) {
		DriverDetailRepoBean bean = new DriverDetailRepoBean();
		Session session = sessionFactory.getCurrentSession();
		bean = session.get(DriverDetailRepoBean.class,id);
		return bean;
	}

}
