package com.nutech.demo.repository;

import org.springframework.stereotype.Repository;

import com.nutech.demo.bean.DriverDetailRepoBean;
import com.nutech.demo.bean.DriverLicenceImageBean;
import com.nutech.demo.bean.DriverPofileImageBean;

@Repository
public interface AdminRepository {

	boolean addDriver(DriverDetailRepoBean bean);

	boolean addLicenceImage(DriverLicenceImageBean driverLicenceImage);

	boolean addProfileImage(DriverPofileImageBean driverPofileImage);

	DriverDetailRepoBean getDriverDetails(int id);

}
