# ProfileDemo
CRUD application in profile using **Spring BOOT** and **Hibernate**

**service methods:**
1.create     -localhost:8080/user/create
2.update     -localhost:8080/user/update
3.login      -localhost:8080/user/login
4.delete     -localhost:8080/user/{id}
5.getuserbyid -localhost:8080/user/{id}
6.getallusers -localhost:8080/user/get

**SQL Query**
    -CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(254) DEFAULT NULL,
  `name` varchar(254) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8
