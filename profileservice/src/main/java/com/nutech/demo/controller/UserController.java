package com.nutech.demo.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.nutech.demo.bean.CabRequestBean;
import com.nutech.demo.bean.DriverAcceptanceBean;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.UserBean;
import com.nutech.demo.bean.UserSubscriptionBean;
import com.nutech.demo.configuration.LoggerImpl;
import com.nutech.demo.service.UserService;

/**
 * @author Aravindan
 * @description User Controller
 */
@RestController
@RequestMapping(value = { "/user" })
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	HttpSession session;

	@Autowired
	JmsTemplate jmsTemplate;

	/**
	 * 
	 * @description It will fetch the user details based on id
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/{profileId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserBean> getUserById(@PathVariable("profileId") String id) {
		LoggerImpl.info("Get user Details :" + id);
		UserBean user = userService.getUserById(id);
		if (user == null) {
			return new ResponseEntity<UserBean>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<UserBean>(user, HttpStatus.OK);
	}

	/**
	 * @description This method is to create a new user
	 * @param user
	 * @param ucBuilder
	 * @return
	 */
	@PostMapping(value = "/create", headers = "Accept=application/json")
	public ResponseEntity<String> createUser(@RequestBody UserBean user, UriComponentsBuilder ucBuilder) {
		LoggerImpl.info("Creating User ::" + user.getUsername());
		ResponseEntity<String> response = null;
		UserBean userExist = userService.findByUsername(user.getUsername());
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getProfileId()).toUri());
		if (userExist == null) {
			userService.createUser(user);
			response = new ResponseEntity<String>("Registered Successfully", headers, HttpStatus.CREATED);
		} else {
			response = new ResponseEntity<String>("Already Exists", headers, HttpStatus.CONFLICT);
		}

		return response;
	}

	/**
	 * @description this method is used for login
	 * @param loginBean
	 * @param ucBuilder
	 * @return
	 */
	@PostMapping(value = "/login", headers = "Accept=application/json")
	public ResponseEntity<String> loginUser(@RequestBody LoginBean loginBean, UriComponentsBuilder ucBuilder) {
		LoggerImpl.info("Login Username ::" + loginBean.getUsername());
		UserBean userBean = userService.loginUser(loginBean);
		String loginStatus = userBean != null ? "success" : "failure";
		session.setAttribute("userId", userBean.getProfileId());
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(userBean.getProfileId()).toUri());
		return new ResponseEntity<String>(loginStatus, headers, HttpStatus.OK);
	}

	/**
	 * @description list of all user
	 * @return userList
	 */
	@GetMapping(value = "/get", headers = "Accept=application/json")
	public List<UserBean> getAllUser() {
		List<UserBean> userList = userService.getUser();
		return userList;

	}

	/**
	 * @description update user
	 * @param currentUser
	 * @return
	 */
	@PutMapping(value = "/update", headers = "Accept=application/json")
	public ResponseEntity<String> updateUser(@RequestBody UserBean user) {
		LoggerImpl.info("Updating User ::" + user.getProfileId());
		UserBean userUpdate = userService.updateUser(user);
		if (userUpdate == null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	/**
	 * @description delete user based on id
	 * @param id
	 * @return
	 */
	@DeleteMapping(value = "/{profileId}", headers = "Accept=application/json")
	public ResponseEntity<UserBean> deleteUser(@PathVariable("profileId") String id) {
		UserBean user = userService.findById(id);
		if (user == null) {
			return new ResponseEntity<UserBean>(HttpStatus.NOT_FOUND);
		}
		userService.deleteUserById(id);
		return new ResponseEntity<UserBean>(HttpStatus.NO_CONTENT);
	}

	/**
	 * @description User Subscription
	 * @param userSubscriptionBean
	 * @return response
	 */
	@PostMapping(value = "/subscribe", headers = "Accept=application/json")
	public ResponseEntity<String> userSubscribe(@RequestBody UserSubscriptionBean userSubscriptionBean) {
		ResponseEntity<String> response = null;
		System.out.println(userSubscriptionBean.getProfileId()+"hetesh");
		UserBean user = userService.findById(userSubscriptionBean.getProfileId());
		if (user != null) {
			System.out.println("1");
			String subcribe = userService.subscribe(user, userSubscriptionBean);
			
			response = new ResponseEntity<String>(subcribe, HttpStatus.CREATED);
		} else {
			response = new ResponseEntity<String>("User Not Exist", HttpStatus.NO_CONTENT);
		}

		return response;
	}

	/**
	 * @description User send cab request to driver
	 * @param cabRequestBean
	 * @return response
	 */
	@PostMapping(value = "/sendCabRequest", headers = "Accept=application/json")
	public ResponseEntity<String> sendCabRequest(@RequestBody CabRequestBean cabRequestBean) {
		System.out.println("Creating Cab request " + cabRequestBean.getProfileId());
		ResponseEntity<String> response = null;
		UserBean userExist = userService.findById(cabRequestBean.getProfileId());
		if (userExist != null) {
			userService.sendCabRequest(cabRequestBean);
			response = new ResponseEntity<String>("Request sended Successfully", HttpStatus.CREATED);
		} else {
			response = new ResponseEntity<String>("User not Exist..", HttpStatus.CONFLICT);
		}
		return response;
	}

	/**
	 * @description User get the response from driver
	 * @return list
	 */
	@GetMapping(value = "/getDriverResponse", headers = "Accept=application/json")
	public List<DriverAcceptanceBean> getDriverResponse() {
		List<DriverAcceptanceBean> list = null;
		String userId = (String) session.getAttribute("userId");
		System.out.println(userId);
		if (userId != null) {
			list = userService.getDriverResponse(userId);
		}
		return list;

	}

	/**
	 * @description User logout
	 * @return res
	 */
	@GetMapping(value = "/logout", headers = "Accept=application/json")
	public String logout() {
		String res = "Please Login First..";
		if (session.getAttribute("userId") != null) {
			session.invalidate();
			res = "Logged Out success..";
		}
		return res;
	}
}
