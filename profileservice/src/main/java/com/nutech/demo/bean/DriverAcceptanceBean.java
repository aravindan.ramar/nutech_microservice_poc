package com.nutech.demo.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Aravindan
 *
 */
@Entity
@Table(name = "driver_response")
public class DriverAcceptanceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int response_id;

	@Column(name = "request_id")
	private int requestId;

	@Column(name = "profile_id")
	private String profileId;

	@Column(name = "userProfile_id")
	private String userProfileId;

	@Column(name = "driver_name")
	private String driverName;

	@Column(name = "gender")
	private String gender;

	@Column(name = "contact_number")
	private String contactNumber;

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getProfileId() {
		return profileId;
	}

	public String getUserProfileId() {
		return userProfileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public int getResponse_id() {
		return response_id;
	}

	public void setResponse_id(int response_id) {
		this.response_id = response_id;
	}

	public void setUserProfileId(String string) {
		this.userProfileId = string;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

}
