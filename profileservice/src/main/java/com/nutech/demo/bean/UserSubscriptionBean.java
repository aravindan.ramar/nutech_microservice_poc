package com.nutech.demo.bean;

/**
 * @author Aravindan
 * @description User Subscription Bean
 */
public class UserSubscriptionBean {
	private String profileId;
	private double amountPaid;
	private int numberOfMonths;

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public int getNumberOfMonths() {
		return numberOfMonths;
	}

	public void setNumberOfMonths(int numberOfMonths) {
		this.numberOfMonths = numberOfMonths;
	}
}
