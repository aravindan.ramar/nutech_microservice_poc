package com.nutech.demo.repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import com.nutech.demo.bean.CabRequestBean;
import com.nutech.demo.bean.DriverAcceptanceBean;
import com.nutech.demo.bean.DriverResponseBean;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.SubscriptionBean;
import com.nutech.demo.bean.UserBean;
import com.nutech.demo.bean.UserSubscriptionBean;
import com.nutech.demo.configuration.LoggerImpl;

/**
 * @author Aravindan
 *
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

	private final static String ACTIVE = "active";
	private final static String UNCHECKED = "unchecked";
	private final static String USERNAME = "username";
	private final static String PASSWORD = "password";
	private static final String HTTP = "http://";
	private static final String SUBSCRIPTION = "/subscription/";
	private static final String SUBSCRIPTION_SERVICE = "subscription-service";
	private static final String SUBSCRIPTION_SUBSCRIBE = "/subscription/subscribe";

	private static final String INPUT_QUEUE = "input-queue";
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private EurekaClient discoveryClient;

	@Autowired
	private JmsTemplate jmsTemplate;

	public void addUser(UserBean user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
	}

	public List<UserBean> getUser() {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings(UNCHECKED)
		List<UserBean> list = session.createCriteria(UserBean.class).list();
		return list;
	}

	public UserBean findById(String id) {
		Session session = sessionFactory.getCurrentSession();
		UserBean user = (UserBean) session.get(UserBean.class, id);
		return user;
	}

	@Override
	public UserBean getUserById(String id) {
		Session session = sessionFactory.getCurrentSession();
		UserBean user = (UserBean) session.get(UserBean.class, id);
		user.setSubscriptionStatus(checkProfileStatus(user));
		return user;
	}

	public UserBean findByUsername(String username) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from UserBean where username=:username");
		query.setParameter("username", username);
		return (UserBean) query.uniqueResult();
	}

	public UserBean updateUser(UserBean userBean) {
		Session session = sessionFactory.getCurrentSession();
		UserBean user = findByUsername(userBean.getUsername());
		user.setPassword(userBean.getPassword());
		user.setFirstname(userBean.getFirstname());
		user.setLastname(userBean.getLastname());
		user.setGender(userBean.getGender());
		user.setAddress(userBean.getAddress());
		user.setCity(userBean.getCity());
		user.setState(userBean.getState());
		user.setCountry(userBean.getCountry());
		user.setPincode(userBean.getPincode());
		user.setMobileNumber(userBean.getMobileNumber());
		session.update(user);
		return user;
	}

	public void delete(String id) {
		Session session = sessionFactory.getCurrentSession();
		UserBean user = findById(id);
		session.delete(user);
	}

	@Override
	public UserBean loginUser(LoginBean loginBean) {
		Session session = sessionFactory.getCurrentSession();
		Criteria loginCriteria = session.createCriteria(UserBean.class);
		Criterion usernameCriterion = Restrictions.eq(USERNAME, loginBean.getUsername());
		Criterion passwordCriterion = Restrictions.eq(PASSWORD, loginBean.getPassword());
		loginCriteria.add(Restrictions.and(usernameCriterion, passwordCriterion));
		UserBean user = (UserBean) loginCriteria.uniqueResult();
		return user;
	}

	@Override
	public boolean checkProfileStatus(UserBean userBean) {
		boolean status = false;
		Application app = discoveryClient.getApplication(SUBSCRIPTION_SERVICE);
		InstanceInfo inst = app.getByInstanceId(SUBSCRIPTION_SERVICE);
		SubscriptionBean subBean = restTemplate.getForObject(
				HTTP + inst.getHostName() + ":" + inst.getPort() + SUBSCRIPTION + userBean.getProfileId(),
				SubscriptionBean.class);
		if (subBean != null) {
			status = subBean.getStatus().equalsIgnoreCase(ACTIVE) ? true : false;
		}
		return status;
	}

	@Override
	public String userSubcribe(SubscriptionBean subBean) {
		Application app = discoveryClient.getApplication(SUBSCRIPTION_SERVICE);
		InstanceInfo inst = app.getByInstanceId(SUBSCRIPTION_SERVICE);
		return restTemplate.postForObject(HTTP + inst.getHostName() + ":" + inst.getPort() + SUBSCRIPTION_SUBSCRIBE,
				subBean, String.class);
	}

	@Override
	public SubscriptionBean subcribeUser(UserBean user, UserSubscriptionBean bean) {
		Date date = new Date();
		SubscriptionBean subBean = new SubscriptionBean();
		subBean.setProfileid(user.getProfileId());
		subBean.setUsername(user.getUsername());
		subBean.setStartdate(date);
		subBean.setEnddate(monthCalculator(date, bean.getNumberOfMonths()));
		return subBean;
	}

	private Date monthCalculator(Date currentDate, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.MONTH, month);
		Date endDate = calendar.getTime();
		return endDate;
	}

	@Override
	public void sendCabRequest(CabRequestBean cabRequestBean) {
		jmsTemplate.convertAndSend("profile-queue", cabRequestBean);
	}

	@JmsListener(destination = "response-queue")
	@Transactional
	public void receiveDriverResponse(DriverResponseBean response) {
		try {
			LoggerImpl.info("Driver Reply :: " + response.getProfileId() + " Driver Name :: "
					+ response.getDriverUserBean().getUsername());
			DriverAcceptanceBean responseBean = new DriverAcceptanceBean();
			responseBean.setProfileId(response.getProfileId());
			responseBean.setRequestId(response.getRequestId());
			responseBean.setUserProfileId(response.getUserProfileId());
			responseBean.setDriverName(response.getDriverUserBean().getUsername());
			responseBean.setGender(response.getDriverUserBean().getGender());
			responseBean.setContactNumber(response.getDriverUserBean().getContactNumber());
			Session session = sessionFactory.getCurrentSession();
			session.save(responseBean);
		} catch (Exception e) {
			LoggerImpl.warn(e.getMessage());
		}

	}

	@Override
	public List<DriverAcceptanceBean> getDriverResponse(String userId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from DriverAcceptanceBean where userProfileId=:userId");
		query.setParameter("userId", userId);
		List<DriverAcceptanceBean> requestlist = query.getResultList();
		return requestlist;
	}
}
