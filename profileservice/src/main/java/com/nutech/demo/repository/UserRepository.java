package com.nutech.demo.repository;

import java.util.List;

import com.nutech.demo.bean.CabRequestBean;
import com.nutech.demo.bean.DriverAcceptanceBean;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.SubscriptionBean;
import com.nutech.demo.bean.UserBean;
import com.nutech.demo.bean.UserSubscriptionBean;

/**
 * @author Aravindan
 *
 */
public interface UserRepository {
	public void addUser(UserBean user);

	public List<UserBean> getUser();

	public UserBean findById(String id);

	public UserBean findByUsername(String username);

	public UserBean loginUser(LoginBean loginBean);

	public UserBean updateUser(UserBean user);

	public boolean checkProfileStatus(UserBean userBean);

	public void delete(String id);

	public String userSubcribe(SubscriptionBean subBean);

	public SubscriptionBean subcribeUser(UserBean user, UserSubscriptionBean bean);

	public UserBean getUserById(String id);

	public void sendCabRequest(CabRequestBean cabRequestBean);

	public List<DriverAcceptanceBean> getDriverResponse(String userId);

}
