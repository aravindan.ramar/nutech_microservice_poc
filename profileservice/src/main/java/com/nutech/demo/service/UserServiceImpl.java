package com.nutech.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.nutech.demo.bean.CabRequestBean;
import com.nutech.demo.bean.DriverAcceptanceBean;
import com.nutech.demo.bean.LoginBean;
import com.nutech.demo.bean.SubscriptionBean;
import com.nutech.demo.bean.UserBean;
import com.nutech.demo.bean.UserSubscriptionBean;
import com.nutech.demo.repository.UserRepository;

/**
 * @author Aravindan
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	private final static String FALLBACKFINDBYID = "fallBackfindById";
	private final static String MOCK = "mock";
	private final static String ID = "mock";
	private final static String FALLBACKSUBSCRIBE = "fallBackSubscribe";

	public List<UserBean> getUser() {
		return userRepository.getUser();
	}

	public UserBean findById(String id) {
		return userRepository.findById(id);
	}

	public UserBean fallBackfindById(String id) {
		UserBean user = new UserBean();
		user.setProfileId(ID);
		user.setUsername(MOCK);
		user.setCountry(MOCK);
		user.setFirstname(MOCK);
		user.setPassword(MOCK);
		return user;

	}

	public UserBean findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public void createUser(UserBean user) {
		userRepository.addUser(user);
	}

	public void deleteUserById(String id) {
		userRepository.delete(id);
	}

	@Override
	public UserBean updateUser(UserBean userBean) {
		return userRepository.updateUser(userBean);
	}

	@Override
	public UserBean loginUser(LoginBean loginBean) {
		UserBean user = userRepository.loginUser(loginBean);
		return user;
	}

	@Override
	@HystrixCommand(fallbackMethod = FALLBACKSUBSCRIBE)
	public String subscribe(UserBean user, UserSubscriptionBean bean) {
		SubscriptionBean subBean = userRepository.subcribeUser(user, bean);
		return userRepository.userSubcribe(subBean);

	}

	public String fallBackSubscribe(UserBean user, UserSubscriptionBean bean) {
		return MOCK;
	}

	@Override

	public UserBean getUserById(String id) {
		return userRepository.getUserById(id);
	}

	@Override
	public void sendCabRequest(CabRequestBean cabRequestBean) {
		userRepository.sendCabRequest(cabRequestBean);
	}

	@Override
	public List<DriverAcceptanceBean> getDriverResponse(String userId) {
		return userRepository.getDriverResponse(userId);
	}
}
