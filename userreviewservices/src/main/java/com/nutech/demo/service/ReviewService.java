package com.nutech.demo.service;

import com.nutech.demo.bean.UserReviewBean;

/**
 * @author Hetesh Mohan
 *
 */
public interface ReviewService {
	/**
	 * @info check if the rating is above 3 it will make comment empty
	 * @param review
	 * @return status
	 */
	public boolean createReview(UserReviewBean review);
}
