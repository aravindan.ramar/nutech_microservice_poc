package com.nutech.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nutech.demo.bean.UserReviewBean;
import com.nutech.demo.repository.UserReviewRepository;

/**
 * @author Hetesh Mohan
 *
 */
@Service
@Transactional
public class ReviewServiceImpl implements ReviewService {
	
	private final static String NULL = null; 
	private static final int COUNT = 3;
	@Autowired
	UserReviewRepository UserReviewRepository;

	
	@Override
	public boolean createReview(UserReviewBean review) 
	{
		if(review.getRating()>COUNT)
		{
			review.setComments(NULL);
		}
		boolean status = UserReviewRepository.addUserReview(review);
		return status;
	}

	



}
