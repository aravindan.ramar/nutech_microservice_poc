package com.nutech.demo.repository;

import com.nutech.demo.bean.UserReviewBean;

/**
 * @author Hetesh Mohan
 *
 */
public interface UserReviewRepository {
	/**
	 * @param review
	 * @return status
	 * @desc to add review details in database
	 */
	public boolean addUserReview(UserReviewBean review);
}
