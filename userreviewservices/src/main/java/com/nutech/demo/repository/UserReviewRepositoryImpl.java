package com.nutech.demo.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nutech.demo.bean.UserReviewBean;

/**
 * @author Hetesh Moaham
 *
 */
@Repository
public class UserReviewRepositoryImpl implements UserReviewRepository {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public boolean addUserReview(UserReviewBean review) {
		boolean success = false;
		try {
		Session session = sessionFactory.getCurrentSession();
		session.save(review);
		success = true;
		}catch (Exception e) {
			
		}
		return success;
	}

}
