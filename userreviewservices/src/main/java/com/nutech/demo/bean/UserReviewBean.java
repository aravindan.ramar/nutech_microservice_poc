package com.nutech.demo.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Hetesh Mohan
 * @desc user Review bean
 *
 */
@Entity
@Table(name = "user_review")
public class UserReviewBean {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int reviewId;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "order_id")
	private String orderId;

	@Column(name = "rating")
	private int rating;

	@Column(name = "comments")
	private String comments;

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
