profilesubscriptionstatus
 
 the service which maintain if the user is subscribe or not,

 port  : 8082
 
 MYSQL
 
 Query :
        CREATE TABLE `profile_status` (
  `subscriptionid` int(11) NOT NULL,
  `profileid` int(11) NOT NULL,
  `username` varchar(254) DEFAULT NULL,
  `status` varchar(254) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  PRIMARY KEY (`subscriptionid`));
  
  
  
  Service methods
            
            1. getByProfileId   - localhost:8082/subscription/{profileId}
            2. getallstatus     - localhost:8082/subscription/getStatus
            3. subscribe        - localhost:8082/subscription/subscribe
   
   