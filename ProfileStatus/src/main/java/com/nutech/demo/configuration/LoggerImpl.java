package com.nutech.demo.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Aravindan
 *
 */
public class LoggerImpl {
	static Logger logger = LoggerFactory.getLogger(LoggerImpl.class);

	public static void info(String info) {
		logger.info(info);
	}

	public static void warn(String warning) {
		logger.warn(warning);
	}
}
