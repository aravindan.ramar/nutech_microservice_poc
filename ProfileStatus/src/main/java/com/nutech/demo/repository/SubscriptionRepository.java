package com.nutech.demo.repository;

import java.util.List;

import com.nutech.demo.bean.SubscriptionBean;

/**
 * @author Aravindan
 *
 */
public interface SubscriptionRepository {
	public void addSubscription(SubscriptionBean subscriptionBean);

	public List<SubscriptionBean> getAllSubscriptionStatus();

	public SubscriptionBean getSubscriptionStatus(String id);

	public SubscriptionBean findByUsername(String username);

	public void renewSubscription(SubscriptionBean subscriptionExist);

}
