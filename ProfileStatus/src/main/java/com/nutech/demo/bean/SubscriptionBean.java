package com.nutech.demo.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.nutech.demo.configuration.SequenceGenerator;

/**
 * @author Aravindan
 * @description SubscriptionBean
 *
 */
@Entity
@Table(name = "profile_status")
public class SubscriptionBean {

	@Id
	@Column(length = 255)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscription_seq")
    @GenericGenerator(
        name = "subscription_seq", 
        strategy = "com.nutech.demo.configuration.SequenceGenerator", 
        
        parameters = {		
        	@Parameter(name = SequenceGenerator.INCREMENT_PARAM, value = "50"),
            @Parameter(name = SequenceGenerator.VALUE_PREFIX_PARAMETER, value = "SUB_"),
            @Parameter(name = SequenceGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	private String subscriptionId;

	@Column(name = "profileid")
	private String profileid;

	@Column(name = "username")
	private String username;

	@Column(name = "startdate")
	private Date startdate;

	@Column(name = "enddate")
	private Date enddate;

	@Column(name = "status")
	private String status;

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}


	public String getProfileid() {
		return profileid;
	}

	public void setProfileid(String profileid) {
		this.profileid = profileid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
