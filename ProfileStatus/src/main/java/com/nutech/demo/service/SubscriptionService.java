package com.nutech.demo.service;

import java.util.List;

import com.nutech.demo.bean.SubscriptionBean;

/**
 * @author Aravindan
 *
 */
public interface SubscriptionService {
	public void createSubscription(SubscriptionBean subscriptionBean );

	public List<SubscriptionBean> getAllSubscriptionStatus();

	public SubscriptionBean getSubscriptionStatus(String profileId);

	public SubscriptionBean findByUsername(String username);

	public void renewSubscription(SubscriptionBean subscriptionExist);
}
