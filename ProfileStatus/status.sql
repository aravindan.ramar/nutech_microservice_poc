CREATE TABLE `profile_status` (
  `subscriptionid` int(11) NOT NULL,
  `profileid` int(11) NOT NULL,
  `username` varchar(254) DEFAULT NULL,
  `status` varchar(254) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  PRIMARY KEY (`subscriptionid`));